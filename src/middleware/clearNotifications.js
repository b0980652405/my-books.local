import store from "@/store/user";
export default function clearNotifications() {

  store.dispatch("messages/resetAllMessages").then();
}
