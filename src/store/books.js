import { defineStore } from 'pinia'
import api from "@/api";

export const useBooksStore = defineStore('books', {
    state: () => ({
        books: [],
    }),
    getters: {
        getBooks: ({books}) => books.items,
    },
    actions: {
        async loadBooks() {
            const books = await api.loadBooks();
            this.books = books.data.data.books;
        },
        setBooks(books) {
            this.books = books;
        }
    },
})
