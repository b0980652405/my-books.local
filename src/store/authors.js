import { defineStore } from 'pinia'
import api from "@/api";

export const useAuthorsStore = defineStore('authors', {
    state: () => ({
        authors: [],
    }),
    getters: {
        getAuthors: ({authors}) => authors.items,
        getPagination: ({ authors }) => {
            const { ...paginationData } = authors;
            return paginationData;
        },
    },
    actions: {
        async loadAuthors(params) {
            const authors = await api.getAuthors(params);
            this.authors = authors.data.data.authors;
        },
        removeAuthor(id) {
            const index = this.authors.items.findIndex((author) => author.id === id);
            if (index !== -1) {
                this.authors.items.splice(index, 1);
            }
        }
    },
})
