import {defineStore} from 'pinia'
import api from "@/api";

export const useBookStore = defineStore('book', {
  state: () => ({
    book: [],
    context: [],
    favorites: [],
    importBook: {
      url: null,
      type: null,
    },
    files: {},
  }),
  getters: {
    getBook: ({book}) => book,
    getContext: ({context}) => context,
    getFavorites: ({favorites}) => favorites,
    getPageNumber: ({book}) => book.current_page_number,
    getTitle: ({book}) => book.title,
    getImportBook: (state) => {
      return {...state.importBook}
    },
    getFiles: ({files}) => files,
  },
  actions: {
    async addBookmark() {
      const selectedText = window.getSelection().focusNode?.textContent;
      if (!selectedText) {
        return;
      }
      let lineObject = {
        page: null,
        items: null
      };

      // Шукаю по context співпадіння з обраним текстом і якщо є, то формую об'єкт
      Object.values(this.context).forEach((line, index) => {
        if (line === selectedText) {
          lineObject.page = this.book.current_page_number;
          lineObject.items = [{index: index + 1, text: line}];
        }
      });

      let isExist = false;
      let favoriteIndex;

      // Перевіряю чи є така сторінка в favorites
      this.favorites.forEach((element, i) => {
        if (element.page === lineObject.page) {
          favoriteIndex = i;
          isExist = true;
        }
      })

      if (!isExist) {
        // Якщо сторінки в favorites немає, то просто додаю об'єкт з обраним текстом ш оновлюю favoriteIndex
        this.favorites.push(lineObject)
        this.favorites.forEach((element, i) => {
          if (element.page === lineObject.page) {
            favoriteIndex = i;
          }
        })
      } else {
        // Якщо сторінка є
        const searchTextIndexInFavorite = Object.values(this.favorites[favoriteIndex].items).findIndex(textItem => textItem === lineObject.items.text);
        if (searchTextIndexInFavorite === -1) {
          this.favorites[favoriteIndex].items.push(lineObject.items[0]);
        }
      }

      api.addFavorite({
        book_id: this.book.id,
        page: lineObject.page,
        favorites: this.favorites[favoriteIndex].items.map(f => f.index)
      }).then(response => {
        return response.statusCode === 200;
      }).catch(error => {
        console.error(error);
      })
    },
    async loadBookWithContextAndFavorites(id) {
      await this.loadBookFromApi(id);
      await this.loadContext(this.book.id, this.book.current_page_number);
      await this.loadFavorites(this.book.id);
    },
    async loadBook(id) {
      await this.loadBookFromApi(id);
    },
    async loadBookFromApi(id) {
      const book = await api.loadBook(id);
      if (book && 200 === book.status) {
        this.book = book.data.data.book;
      }
    },
    async loadContext(bookId, pageNumber) {
      const response = await api.getContext(bookId, pageNumber);

      if (response.data) {
        if (response.data.data.page.length) {
          this.context = response.data.data.page[0].text;
          this.book.current_page_number = pageNumber;
        }
      }
    },
    async loadFavorites(bookId) {
      const response = await api.getFavorites(bookId);

      if (response.data) {
        this.favorites = response.data.data.data;
      }
    },
    removeFavorite(itemForRemove) {
      // Видалити в store
      let favoriteItem = this.favorites.find(item => item.page === itemForRemove.page);
      const favorites = favoriteItem.items.filter(item => item.index !== itemForRemove.favorite.index)

      // Відправити запит на бекенд
      api.addFavorite({
        book_id: this.book.id,
        page: itemForRemove.page,
        favorites: favorites.map(f => f.index)
      }).then(response => {
        if (response.status === 201) {
          favoriteItem.items = favorites;
          if (!favoriteItem.items.length) {
            this.favorites.splice(favoriteItem, 1)
          }
        }
      }).catch(error => {
        console.error(error);
      })
    },
    setImportBook(importBook) {
      this.importBook = {...importBook};
    },
    setFiles(files) {
      this.files = {...files};
    }
  },
})
