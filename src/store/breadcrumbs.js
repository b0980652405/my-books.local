import { defineStore } from 'pinia'

export const useBreadcrumbsStore = defineStore('breadcrumbs', {
    state: () => ({
        breadcrumbs: [],
    }),
    getters: {
        getBreadcrumbs: (state) => state.breadcrumbs,
    },
    actions: {
        setBreadcrumbs(breadcrumbs) {
            this.breadcrumbs = breadcrumbs;
        },
    },
})
