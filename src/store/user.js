import { defineStore } from 'pinia'
import api from "@/api";

export const useUserStore = defineStore('user', {
  state: () => ({
    user: [],
  }),
  getters: {
    getUser: ({ user }) => user,
  },
  actions: {
    async loadUser() {
      const user = await api.getCurrentUser();
      this.user = user.data.data.user;
    },
    setUserPhoto(path) {
      this.user.image = path;
    },
  },
})
