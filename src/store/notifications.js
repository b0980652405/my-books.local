import { defineStore } from 'pinia'
import api from "@/api";

export const useNotificationsStore = defineStore('notifications', {
    state: () => ({
        notifications: [],
    }),
    getters: {
        getNotifications: ({ notifications }) => notifications,
    },
    actions: {
        addNotifications(notifications) {
            this.notifications = [...this.notifications, notifications];
        },
        async loadNotifications() {
            const notifications = await api.loadNotifications();
            this.notifications = notifications.data.data.notifications;
        },
        async clearNotifications() {
            const response = await api.clearNotifications();
            if (response.status === 200) {
                this.notifications = [];
            }
        },
    },
})
