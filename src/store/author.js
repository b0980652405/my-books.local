import { defineStore } from 'pinia'
import api from "@/api";

export const useAuthorStore = defineStore('author', {
    state: () => ({
        author: [],
    }),
    getters: {
        getAuthor: ({author}) => author.items,
    },
    actions: {
        async loadAuthor(id) {
            const author = await api.getAuthorFromApi(id);
            this.author = author.data.data.author;
        },
    },
})
