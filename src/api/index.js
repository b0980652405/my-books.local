import axios from "axios";
import cookies from "vue-cookies";

axios.defaults.baseURL = process.env.VUE_APP_BACKEND_URL + "api";
axios.defaults.headers.common["Accept-Language"] =
  process.env.VUE_APP_LANGUAGE === "ua" ? "ua" : "en";

axios.interceptors.request.use((config) => {
  const token = cookies.get("auth_token");
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

function handleApiError(error) {
  if (404 === error.request.status) {
    window.location.href = '/404';
  }

  if (500 === error.request.status) {
    window.location.href = '/500';
  }

  console.error("Помилка запиту:", error.response.data.errors[0]);
  throw error;
}

export default {
  // Method for create get params
  createQueryString(queryParam, params) {
    if (params.currentPage) {
      queryParam += queryParam.length > 0 ? '&currentPage=' + params.currentPage : '?currentPage=' + params.currentPage;
    }
    if (params.perPage) {
      queryParam += queryParam.length > 0 ? '&perPage=' + params.perPage : '?perPage=' + params.perPage
    }
    return queryParam;
  },

  // Auth
  registerUser(data) {
    return axios.post("/auth/register", data).catch(handleApiError);
  },
  loginSend(data) {
    return axios.post("/auth/login", data).catch(handleApiError);
  },

  // Authors
  getAuthorFromApi(id) {
    return axios.get("/author/" + id).catch(handleApiError);
  },
  getAuthors(params) {
    return axios.get("/authors" + this.createQueryString('', params)).catch(handleApiError);
  },
  removeAuthor(id) {
    return axios.delete('/author/' + id).catch(handleApiError);
  },
  storeAuthor(params) {
    return axios.post("/author", {...params}).catch(handleApiError);
  },
  updateAuthor(id, params) {
    return axios.put("/author/" + id, {...params}).catch(handleApiError);
  },

  // Book
  storeBook(params) {
    return axios.post("/book", params).catch(handleApiError);
  },
  storeBookPdf(params) {
    return axios.post("/book/files/pdf", params).catch(handleApiError);
  },
  storeBookImage(params) {
    return axios.post("/book/files/image", params).catch(handleApiError);
  },
  loadBook(id) {
    return axios.get('/book/' + id).catch(handleApiError);
  },
  loadBooks() {
    return axios.get('/books?orderBy=id,desc').catch(handleApiError);
  },
  removeBook(id) {
    return axios.delete('/book/' + id).catch(handleApiError);
  },

  // User
  getCurrentUser() {
    return axios.get("/user").catch(handleApiError);
  },
  updateCurrentUser(params) {
    return axios.put('/user/' + params.id, params).catch(handleApiError);
  },
  updateUserPhoto(params) {
    return axios.post('/user/photo', params).catch(handleApiError);
  },

  // Import
  importBook(params) {
    return axios.post('/import', params).catch(handleApiError);
  },

  // Notifications
  loadNotifications() {
    return axios.get("/notifications").catch(handleApiError);
  },
  clearNotifications() {
    return axios.get("/notifications/clear").catch(handleApiError);
  },

  // Reader
  getContext(bookId, pageNumber) {
    return axios.get("/book/" + bookId + "/context/" + pageNumber).catch(handleApiError);
  },
  addFavorite(params) {
    return axios.post('/book/favorites', params).catch(handleApiError);
  },
  getFavorites(bookId) {
    return axios.get('/book/' + bookId + '/favorites').catch(handleApiError);
  },

  // Queue
  addToQueue(params) {
    return axios.post('/queue', params).catch(handleApiError);
  },
  loadQueue() {
    return axios.get("/queue").catch(handleApiError);
  },
  changeOrderQueue(params) {
    return axios.put('/queue', {order: params}).catch(handleApiError);
  },
  removeBookFromQueue(id) {
    return axios.delete('/queue/' + id).catch(handleApiError);
  },

  // Search
  search(text) {
    return axios.get("/search?text=" + text).catch(handleApiError);
  }
};
