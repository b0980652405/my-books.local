import { createRouter, createWebHistory } from "vue-router";
import log from "@/middleware/log";
import auth from "@/middleware/auth";
import AuthorCreate from "@/components/Author/AuthorCreate.vue";
import AuthorsList from "@/views/Authors/AuthorsList.vue";
import BookCreate from "@/views/Book/BookCreate.vue";
import BookDetail from "@/views/Book/BookDetail.vue";
import BooksList from "@/views/Books/BooksList.vue";
import BookReaderDB from "@/views/Reader/BookReaderDB.vue";
import Error404 from "@/views/Errors/Error404.vue";
import Error500 from "@/views/Errors/Error500.vue";
import Login from "@/views/Auth/Login.vue";
import QueueList from "@/views/Queue/QueueList.vue";
import Registration from "@/views/Auth/Registration.vue";
import UserProfile from "@/views/User/UserProfile.vue";
import AuthorEdit from "@/components/Author/AuthorEdit.vue";
import BookReaderPDF from "@/views/Reader/BookReaderPDF.vue";

const routes = [
  // Auth
  {path: "/registration", name: "Registration", component: Registration, meta: { middleware: [log] },},
  {path: "/login", name: "Login", component: Login, meta: { middleware: [log] },},

  // Authors
  {path: "/author/create", name: "AuthorCreate", component: AuthorCreate, meta: { middleware: [auth, log] },},
  {path: "/author/:id", name: "AuthorEdit", component: AuthorEdit, meta: { middleware: [auth, log] },},
  {path: "/authors", name: "AuthorsList", component: AuthorsList, meta: { middleware: [auth, log] },},

  // Books
  {path: "/", name: "BooksList", component: BooksList, meta: { middleware: [auth, log] },},
  {path: "/book/create", name: "BookCreate", component: BookCreate, meta: { middleware: [auth, log] },},
  {path: "/book/:id", name: 'BookDetail', component: BookDetail, meta: { middleware: [auth, log] }},
  {path: "/book/:id/reader/db", name: 'BookReaderDB', component: BookReaderDB, meta: { middleware: [auth, log] }},
  {path: "/book/:id/reader/pdf", name: 'BookReaderPDF', component: BookReaderPDF, meta: { middleware: [auth, log] }},

  // Queue
  {path: "/queue", name: "QueueList", component: QueueList, meta: { middleware: [auth, log] },},

  // User
  {path: "/user/profile", name: "UserProfile", component: UserProfile, meta: { middleware: [auth, log] },},

  {path: "/404", name: 'Error404', component: Error404, meta: {requireAuth: false,},},
  {path: "/500", name: 'Error500', component: Error500, meta: {requireAuth: false,},},
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

// Middleware runner
router.beforeEach((to, from, next) => {
  if (!to.meta.middleware) {
    return next();
  }

  to.meta.middleware.forEach((item) => {
    item({to, from, next});
  });

  return next();
});

export default router;
